import math
import sys, os
import cv2
import argparse
import time

def find_parent_path(level=0):
    parent = os.path.abspath(__file__)
    for i in range(level):
        parent = os.path.dirname(parent)

    return parent

sys.path.append(find_parent_path(level=2))

from camera_handler.CameraCalibration import CameraCalibration
from camera_handler.CameraHandler import CameraHandler
from gpio_controller.ServoController import ServoController as Controller
from autonomous_driving.lane_detection.LaneDetectionPolynomial import LaneDetectionPolynomial
from config import get_config

import threading

speed_add = 1
theta_add = 20

class ThreadLaneDetection (threading.Thread):
    def __init__(self, threadLock, set_img_fn, is_stop_fn, config=None, lane_detection=None):
        threading.Thread.__init__(self)

        self.set_img_fn = set_img_fn
        self.is_stop_fn = is_stop_fn 

        self.threadLock = threadLock

        self.config = config
        if lane_detection is None:
            self.lane_detection = LaneDetectionPolynomial(config=config, isTest=True)

        self.camera = CameraHandler()
        #TODO: Implement camera calibration - not yet implemented
        #self.camera_calibration = CameraCalibration()

    
    def init(self, init_frame):
        (h, w) = init_frame.shape[:2]
        if self.config is None:
            self.config = get_config(w, h)
            self.lane_detection.init(init_frame)

    def run(self):
        init = True
        while True:
            frame = self.camera.get_frame()

            if init:
                self.init(frame)
                init = False

            center_point, img = self.lane_detection.find_lanes(frame)

            self.threadLock.acquire()
            self.set_img_fn(img, frame)
            self.threadLock.release()

            self.threadLock.acquire()
            isStop = self.is_stop_fn()
            self.threadLock.release()

            if isStop:
                print("Stopping in Find Lanes")
                break

class SelfDriver:
    def __init__(self, config=None, lane_detection=None, isSave=False):
        self.config = config
        self.isStop = False
        self.img = None
        self.imgOrigin = None
        
        self.lane_detection = lane_detection
        self.threadLock = threading.Lock()
        self.threadLane = None

        self.controller = Controller()
        self.speed = 0 
        self.theta = 0 
        self.oldSpeed = 0
        self.oldTheta = 0

        self.isSave = isSave
        self.saver = None
        self.originSaver = None
        self.controlSaver = None

    def stop(self):
        print("Stopping control")
        self.controller.set_speed(0)
        self.controller.set_steer(0)
        if self.isSave:
            self.saver.release()
            self.originSaver.release()

        self.threadLane.camera.release()

        print("Drive Stopped")



    def set_img(self, img, imgOrigin):
        self.img = img
        self.imgOrigin = imgOrigin
    
    def get_img(self):
        return self.img, self.imgOrigin

    def set_stop(self, isStop=True):
        self.isStop = isStop

    def get_stop(self):
        return self.isStop

    def init(self):
        self.controller.init()
        self.threadLane = ThreadLaneDetection(config=self.config, 
                                              lane_detection=self.lane_detection, 
                                              threadLock=self.threadLock, 
                                              set_img_fn=self.set_img,
                                              is_stop_fn=self.get_stop)

    def run(self):
        self.init()
        self.threadLane.start()
        frame_index = 0

        while (True):
            self.threadLock.acquire()
            frame, origin = self.get_img()
            self.threadLock.release()

            if frame is not None:
                if self.isSave:
                    if self.saver is None:
                        h, w = frame.shape[:2]
                        fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
                        output_dir = 'output_' + str(time.time())
                        try:
                            os.mkdir(output_dir)
                        except Exception as e:
                            print(e)

                        self.saver = cv2.VideoWriter(os.path.join(output_dir, "./self_drive.avi"),fourcc, 40.0, (w, h))
                        self.originSaver = cv2.VideoWriter(os.path.join(output_dir, "./self_drive_origin.avi"),fourcc, 40.0, (w, h))
                        self.controlSaver = open(os.path.join(output_dir, "control.txt"), "w")
                        self.controlSaver.write("Frame_Index,Old_Speed,Old_Theta,New_Speed,New_Theta")
                        self.controlSaver.write("\n")

                    self.originSaver.write(origin)
                    self.saver.write(frame)
                    self.controlSaver.write("%d, %2.2f, %2.2f, %2.2f, %2.2f" % (frame_index, self.oldSpeed, self.oldTheta, self.speed, self.theta))
                    self.controlSaver.write("\n")
                    
                    frame_index += 1
            
                cv2.imshow("img", frame)
                char = cv2.waitKey(1) & 0xFF
                
                if (chr(char) == 'w' or char == 82):
                    print("Forward")
                    self.speed += speed_add
                
                elif (chr(char) == 's' or char == 84):
                    print("Backward")
                    self.speed -= speed_add

                elif (chr(char) == 'd' or char == 83):
                    print("Steer left")
                    self.theta += theta_add

                elif (chr(char) == 'a' or char == 81):
                    print("Steer right")
                    self.theta -= theta_add

                elif (char == ord('\n') or char == ord('\r')):
                    print("Stop")
                    self.theta = 0
                    self.speed = 0

                elif (chr(char) == 'q'):
                    print("Stopping")
                    self.threadLock.acquire()
                    self.set_stop(True)
                    self.threadLock.release()

                    break
            
                if (self.oldSpeed != self.speed) or (self.oldTheta != self.theta):
                    print("Speed:", self.speed, "Theta:", self.theta)
                    self.oldSpeed = self.speed
                    self.oldTheta = self.theta

                    self.speed = self.controller.set_speed(self.speed)
                    self.theta = self.controller.set_steer(self.theta)


        self.threadLane.join()
        self.stop()


if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("-s", "--isSave", 
        default=False, 
        help="Specify whether the driver save output to video or not")

    args = vars(ap.parse_args())

    driver = SelfDriver(isSave=args["isSave"])
    try:
        driver.run()
    except KeyboardInterrupt:
        driver.stop()
        print("Main Stopped")
        sys.exit()
    except Exception as e:
        print(e)
        print("Main stopped")
        sys.exit()






