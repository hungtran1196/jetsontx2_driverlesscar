from autonomous_driving.AutonomousDriver import AutonomousDriver
from manual_driving.SelfDriver import SelfDriver
from autonomous_driving.lane_detection.LaneDetectionHough import LaneDetectionHough
from autonomous_driving.lane_detection.LaneDetectionPolynomial import LaneDetectionPolynomial

from config import get_config
import argparse
import sys
import cv2

def show_img(img):
    cv2.imshow("output", img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        return True

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--isSave", 
        default=False, 
        help="Specify whether the driver save output to video or not")

    ap.add_argument("-t", "--isTest", 
        default=False, 
        help="Specify whether the driver should show image while driving or not")

    ap.add_argument("-bs", "--baseSpeed", 
        default=40, 
        help="Specify the base speed of the car")

    ap.add_argument("-m", "--mode",
        default=1,
        help="Specify the mode of driver (default: automatic = 1, manual = 0)")

    ap.add_argument("-lm", "--laneMode",
        default=-1,
        help="Specify the mode of lane detection (default: polynomial = 1, hough_line = 0)")

    driver = None 

    args = vars(ap.parse_args())

    if (args["mode"] == 1 or args["mode"] == '1'):
        driver = AutonomousDriver(isTest=args["isTest"], isSave=args["isSave"], base_speed=args["baseSpeed"])
        
        try:
            if args["isTest"]:
                driver.run(show_img)
            else:
                driver.run()
        except KeyboardInterrupt:
            driver.stop()
            print("Stopped")
            exit()

    elif (args["mode"] == '0'):
        driver = SelfDriver(isSave=args["isSave"])
        try:
            driver.run()
        except KeyboardInterrupt:
            driver.stop()
            print("Stopped")
            sys.exit()
    else:
        print("Wrong mode !")
        sys.exit()
    