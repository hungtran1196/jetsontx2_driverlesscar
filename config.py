import numpy as np

DEFAULT_WIDTH = 1280
DEFAULT_HEIGHT = 720 

DEFAULT_CONFIG = {
    "sliding_windows": {
        "nwindows": 10,
        "margin": 80,
        "minpix": 40
    },
    "boundary": {
        "x2": 100.0,
        "x1": 1.0,
        "x0": 0.001,
        "nkeep": 5,
        "lane_pixel_diff": 350,
        "lane_pixel_diff_error": 100,
    },
    "transform": {
        "width": DEFAULT_WIDTH,
        "height": DEFAULT_HEIGHT,
        "src": np.float32([
            (545, 464),
            (737, 464),
            (228, 682),
            (1079, 682)
        ]),
        "dst": np.float32([
            (450, 0),
            (DEFAULT_WIDTH - 450, 0),
            (450, DEFAULT_HEIGHT),
            (DEFAULT_WIDTH - 450, DEFAULT_HEIGHT)
        ])
    },
    "center": {
        "y": DEFAULT_HEIGHT * 2 // 3,
        "offset": 200
    },
    "text_img_polynomial": {
        "x": 40,
        "y": 380,
        "offset": 20
    }
}

def get_config(w, h):
    config = DEFAULT_CONFIG
    config["transform"]["width"] = w
    config["transform"]["height"] = h
    config["transform"]["dst"] = np.float32([
            (450, 0),
            (w - 450, 0),
            (450, h),
            (w - 450, h)
    ])
    config["transform"]["src"] = np.float32([
            (545*w//DEFAULT_WIDTH, 464*h//DEFAULT_HEIGHT),
            (737*w//DEFAULT_WIDTH, 464*h//DEFAULT_HEIGHT),
            (228*w//DEFAULT_WIDTH, 682*h//DEFAULT_HEIGHT),
            (1079*w//DEFAULT_WIDTH, 682*h//DEFAULT_HEIGHT)

    ])
    config["center"]["y"] = h * 2 // 3
    config["text_img_polynomial"]["x"] = 40*w//DEFAULT_WIDTH
    config["text_img_polynomial"]["y"] = 380*h//DEFAULT_HEIGHT
    config["text_img_polynomial"]["offset"] = 20*h//DEFAULT_HEIGHT

    return config
