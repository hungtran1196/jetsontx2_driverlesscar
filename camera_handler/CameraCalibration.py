import cv2
import pickle
import matplotlib.pyplot as plt
import numpy as np
import sys, os

dir_path = os.path.dirname(os.path.abspath(__file__))
images_path = os.path.join(dir_path, 'calibration_img')
data_path = os.path.join(dir_path, 'data')

class CameraCalibration:
    def __init__(self, num_rows=6, num_cols=8, have_data=True):
        self.num_cols = num_cols
        self.num_rows = num_rows

        if have_data:
            self.data = pickle.load(open(os.path.join(data_path, "camera_calibration.p"), "rb"))

    def calibrate(self, show_img=False):
        objp = np.zeros((self.num_rows*self.num_cols,3), np.float32)
        objp[:,:2] = np.mgrid[0:self.num_rows,0:self.num_cols].T.reshape(-1,2)

        # create lists to store imagepoints and objectpoints
        objpoints = []
        imgpoints = []

        #Termination criteria
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        if show_img:
            #For drawing the images
            plt.figure(figsize = (20, 5))
            plt.title('calibration')

        images = os.listdir(images_path)

        #Loop and search for chessboard corner
        for i, frame in enumerate(images):
            print(frame)
            img = cv2.imread(os.path.join(images_path, frame))

            #Turn color image to gray image
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            plt.subplot(4, 5, i+1)

            #Find the corner of chessboard
            ret, corners = cv2.findChessboardCorners(gray, (self.num_rows, self.num_cols), None)

            if ret == True:

                if show_img:
                    cv2.drawChessboardCorners(img, (self.num_rows, self.num_cols), corners, ret)

                objpoints.append(objp)
                imgpoints.append(corners)

            if show_img:
                cv2.imshow("img", img)
                cv2.waitKey(0)
        
        cv2.destroyAllWindows()
        img = cv2.imread(os.path.join(images_path, images[0]))
        img_size = (img.shape[1], img.shape[0])
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img_size, None, None)

        data = {
            'mtx': mtx,
            'dist': dist
        }

        pickle.dump(data, open(os.path.join(data_path, "camera_calibration.p"), "wb"))

    def undistort(self, image):
        return cv2.undistort(image, self.data.mtx, self.data.dist, None, self.data.mtx)


if __name__ == '__main__':
    print("Begin")
    camera_calibration = CameraCalibration(have_data=False)
    camera_calibration.calibrate(False)
                





