import numpy
import cv2

class CameraHandler:

    def __init__(self):
        """Using opencv to grab image from webcame and
           return a byte-like image
        """

        self.video = cv2.VideoCapture(1)
        self.compression_params = [int(cv2.IMWRITE_JPEG_QUALITY), 20]

    def __del__(self):
        self.video.release()

    def get_frame_bytes(self):
        image = self.get_frame
        #read method capture raw images, we need to convert it to jpeg
        ret, jpeg = cv2.imencode(".jpg", image, self.compression_params)
        return jpeg.tobytes()

    def release(self):
        self.video.release()

    def get_frame(self):
        _, image = self.video.read()
        if image is None:
            return

        return image
        
