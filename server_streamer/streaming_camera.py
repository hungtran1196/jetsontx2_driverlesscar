from flask import Flask, jsonify, render_template, Response, send_from_directory, request
import sys, os
print (os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from camera_handler.camera_handler import CameraHandler as Camera
from gpio_controller.servo_control import servo_controller

app = Flask(__name__)
servo_controller = servo_controller()
servo_controller.init()

speed = 0
theta = 0
speed_add = 10
theta_add = 20

def generate_image(Camera):
    while True:
        frame = Camera.get_frame()
        if frame is None:
            continue
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

@app.route("/")
def index():
    return render_template("index.html")


@app.route("/video_feed")
def video_feed():
    return Response(generate_image(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route("/control", methods=["POST"])
def control():
    global speed
    global theta
    command = request.form['command']
    if command == "up":
        speed += speed_add
    elif command == "down":
        speed -= speed_add
    elif command == "left":
        theta += theta_add
    elif command == "right":
        theta -= theta_add

    theta = servo_controller.get_theta_in_boundary(theta)
    speed = servo_controller.get_speed_in_boundary(speed)

    print (speed, theta, command)
    
    servo_controller.set_speed(speed)
    servo_controller.set_steer(theta)
    
    return jsonify({
            "speed": speed,
            "theta": theta
        })


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
