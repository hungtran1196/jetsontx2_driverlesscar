import numpy as np 
import cv2 

def stack_images(img1, img2, axis=0, resize=True):
    result = None
    height, width = img1.shape[:2]

    if axis == 0:
        result = np.hstack((img1, img2))
        if resize:
            result = cv2.resize(result, (width, int(height/2)))

    else:
        result = np.vstack((img1, img2))
        if resize:
            result = cv2.resize(result, (int(width/2), height))

    return result