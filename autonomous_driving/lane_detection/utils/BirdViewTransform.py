import sys, os

def find_parent_path(level=0):
    parent = os.path.abspath(__file__)
    for i in range(level):
        parent = os.path.dirname(parent)

    return parent

sys.path.append(find_parent_path(level=4))

import cv2
import argparse
import numpy as np
import scipy
from autonomous_driving.lane_detection.utils.utils import stack_images
from config import get_config

class BirdViewTransform:
    def __init__(self, init_img, config=None, isTest=False):
        self.isTest = isTest

        h, w = init_img.shape[:2]

        if config is None:
            self.config = get_config(w, h)["transform"]
        else:
            self.config = config

        self.M = cv2.getPerspectiveTransform(self.config["src"], self.config["dst"]) 
        self.Minv = cv2.getPerspectiveTransform(self.config["dst"], self.config["src"])
    
    def transform(self, img):

        unwrapped_img = cv2.warpPerspective(img, self.M, (self.config["width"], self.config["height"]), flags=cv2.INTER_LINEAR)

        if self.isTest:
            origin = img.copy()
            src = np.array(self.config["src"], np.int32)
            temp = src[2].copy()
            src[2], src[3] = src[3], temp
            src = src.reshape((-1, 1, 2))
            cv2.polylines(origin, [src], True, (0, 200, 100), thickness=5)

            return unwrapped_img, origin

        return unwrapped_img, None

if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("-vid", "--isVideo", 
        default=False, 
        help="Specify whether the input is video or image")
    
    ap.add_argument("-i", "--input", required=True,
        help="Specify the input of the image / video")

    ap.add_argument("-o", "--output", 
        default=".",
        help="Specify the output dir")
    
    ap.add_argument("-f", "--file", 
        help="Specify the file name")



    args = vars(ap.parse_args())

    input_path = args["input"]
    filename = args["file"]
    output_dir = args["output"]

    if not args["isVideo"]:
        if filename is None:
            filename = "output.jpg"
        output_path = os.path.join(output_dir, filename)

        img = cv2.imread(input_path)

        transformer = BirdViewTransform(init_img=img, isTest=True)
        unwrapped, origin = transformer.transform(img)

        output = stack_images(origin, unwrapped)

        cv2.imshow("output", output)

        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite(output_path, output)

    else:
        if filename is None:
            filename = "output.avi"
        output_path = os.path.join(output_dir, filename)

        cap = cv2.VideoCapture(input_path)
        fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
        video_width = int(cap.get(3))
        video_height = int(cap.get(4)/2)

        out = cv2.VideoWriter(output_path,fourcc, 40.0, (video_width, video_height))
        init_transformer = True
        transformer = None
        while(cap.isOpened()):
            ret, frame = cap.read()
            if not ret:
                break

            if init_transformer:
                transformer = BirdViewTransform(init_img=frame, isTest=True)

            unwrapped, origin = transformer.transform(frame)

            output = stack_images(origin, unwrapped)

            cv2.imshow("output", output)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            out.write(output)

        cap.release()
        out.release()
    

