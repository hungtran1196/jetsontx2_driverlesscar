#!/bin/bash
import sys, os

def find_parent_path(level=0):
    parent = os.path.abspath(__file__)
    for i in range(level):
        parent = os.path.dirname(parent)

    return parent

sys.path.append(find_parent_path(level=4))


import cv2
import argparse
import numpy as np

from autonomous_driving.lane_detection.utils.utils import stack_images

class YellowWhiteDetection:
    def __init__(self):
        pass
    
    def _detect_white(self, image):
        thresh = (220, 255)
        # 1) Convert to HLS color space
        hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
        hls_l = hls[:,:,1]
        hls_l = hls_l*(255/np.max(hls_l))
        # 2) Apply a threshold to the L channel
        binary_output = np.zeros_like(hls_l)
        binary_output[(hls_l > thresh[0]) & (hls_l <= thresh[1])] = 1
        # 3) Return a binary image of threshold result
        return binary_output
        
    def _detect_yellow(self, image):
        thresh=(190,255)
        # 1) Convert to LAB color space
        lab = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)
        lab_b = lab[:,:,2]
        # don't normalize if there are no yellows in the image
        if np.max(lab_b) > 175:
            lab_b = lab_b*(255/np.max(lab_b))
        # 2) Apply a threshold to the L channel
        binary_output = np.zeros_like(lab_b)
        binary_output[((lab_b > thresh[0]) & (lab_b <= thresh[1]))] = 1
        # 3) Return a binary image of threshold result
        return binary_output

    def detect(self, image):
        img_white = self._detect_white(image)
        img_yellow = self._detect_yellow(image)
    
        # Combine HLS and Lab B channel thresholds
        combined = np.zeros((img_yellow.shape[0], img_yellow.shape[1]))
        combined[(img_white == 1) | (img_yellow == 1)] = 1
        combined = np.uint8(combined*255)

        return combined



if __name__ == '__main__':
    ap = argparse.ArgumentParser()

    ap.add_argument("-vid", "--isVideo", 
        default=False, 
        help="Specify whether the input is video or image")
    
    ap.add_argument("-i", "--input", required=True,
        help="Specify the input of the image / video")

    ap.add_argument("-o", "--output", 
        default=".",
        help="Specify the output dir")
    
    ap.add_argument("-f", "--file", 
        help="Specify the file name")



    args = vars(ap.parse_args())

    input_path = args["input"]
    filename = args["file"]
    output_dir = args["output"]

    if not args["isVideo"]:
        if filename is None:
            filename = "output_color.jpg"
        output_path = os.path.join(output_dir, filename)

        img = cv2.imread(input_path)

        color_detection = YellowWhiteDetection()
        output = color_detection.detect(img)

        output = np.stack((output, output, output), axis=2)

        output = stack_images(img, output, axis=1)

        cv2.imshow("Output", output)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite(output_path, output)

    else:
        if filename is None:
            filename = "output_color.avi"
        output_path = os.path.join(output_dir, filename)

        cap = cv2.VideoCapture(input_path)
        fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
        video_width = int(cap.get(3))
        video_height = int(cap.get(4)/2)

        out = cv2.VideoWriter(output_path,fourcc, 40.0, (video_width, video_height))

        color_detection = YellowWhiteDetection()
        while(cap.isOpened()):
            ret, frame = cap.read()
            if not ret:
                break

            output = color_detection.detect(frame)
            output = np.stack((output, output, output), axis=2)

            output = stack_images(frame, output)

            out.write(output)

            cv2.imshow("Output", output)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
                

        cap.release()
        out.release()


            