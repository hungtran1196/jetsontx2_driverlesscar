#!/bin/bash
import sys, os

def find_parent_path(level=0):
    parent = os.path.abspath(__file__)
    for i in range(level):
        parent = os.path.dirname(parent)

    return parent

sys.path.append(find_parent_path(level=3))

import argparse
import numpy as np
import cv2
from autonomous_driving.lane_detection.utils.utils import stack_images

class LaneDetectionHough:
    def __init__(self, img_w, img_h, isTest = False):
        self.isTest = isTest

        self.roi_triangle = {
            "peak": (int(img_w/2), int(img_h/2)),
            "bottom_left": (int(img_w/8), img_h),
            "bottom_right": (int(img_w*7/8), img_h)
        }

        self.roi_left = {
            "start_x": int(0),
            "start_y": int(img_h/2) + int(img_h/8),
            "width": int(img_w/2),
            "height": int(img_h/2) - int(img_h/8)
        }

        self.roi_right = {
            "start_x": int(img_w/2),
            "start_y": int(img_h/2) + int(img_h/8),
            "width": int(img_w/2),
            "height": int(img_h/2) - int(img_h/8)        
        }
    
    def triangle_crop(self, img):
        mask = np.zeros_like(img)
        if len(img.shape) > 2:
            channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
            ignore_mask_color = (255,) * channel_count
        else:
            ignore_mask_color = 255
        
        vertices = np.array([[
            self.roi_triangle["bottom_left"],
            self.roi_triangle["peak"],
            self.roi_triangle["bottom_right"]
        ]], dtype=np.int32)
        
        #filling pixels inside the polygon defined by "vertices" with the fill color    
        cv2.fillPoly(mask, vertices, ignore_mask_color)
        masked_image = cv2.bitwise_and(img, mask)
        return masked_image



    def crop(self, image):
        img = self.triangle_crop(image)

        left = img[
            self.roi_left["start_y"]:self.roi_left["start_y"] + self.roi_left["height"],
            self.roi_left["start_x"]:self.roi_left["start_x"] + self.roi_left["width"],
        ]

        right = img[
            self.roi_right["start_y"]:self.roi_right["start_y"] + self.roi_right["height"],
            self.roi_right["start_x"]:self.roi_right["start_x"] + self.roi_right["width"],
        ]

        return left, right

    def grayscale(self, img):
        return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    def gaussianBlur(self, img, kernelSize=3):
        return cv2.GaussianBlur(img, (kernelSize, kernelSize), 0)

    def canny(self, img, low_thresh, high_thresh):
        return cv2.Canny(img, low_thresh, high_thresh)

    def hough_lines(self, img, rho=1, 
            theta=np.pi/180, threshold=15, 
            min_line_len=1, max_line_gap=1):
        lines = cv2.HoughLinesP(img, rho, theta, threshold, np.array([]), min_line_len, max_line_gap)
        return lines

    def average_lines(self, lines, end_y, is_left):
        lines = lines.tolist()
        lines = [i for sublist in lines for i in sublist]

        lines = pd.DataFrame(lines, columns='x1 y1 x2 y2'.split(" "))
        lines["slope"] = (lines["y1"] - lines["y2"]) / (lines["x1"] - lines["x2"])
        lines["length"] = abs(lines["y1"] - lines["y2"])

        if is_left:
            lines = lines[(lines["slope"]<0) & (lines["slope"]>-0.9)]
            
            sorted_lines = lines.sort_values(by='length')
            begin_y = sorted_lines['y1'].iloc[-1]
            begin_x = lines[lines['y1'] == begin_y]['x1'].median()
            # begin_x, begin_y = lines.sort_values(by='y1')[['x1', 'y1']].iloc[0]
            begin_x, begin_y = int(begin_x), int(begin_y)
            
            mean_slope = lines["slope"].median()
            end_x = ((end_y - begin_y) / mean_slope) + begin_x
            end_x = int(end_x)
            return ((begin_x, begin_y), (end_x, end_y))

        else:
            lines = lines[(lines["slope"]>0) & (lines["slope"]<0.9)]
            
            sorted_lines = lines.sort_values(by='length')

            begin_y = sorted_lines['y1'].iloc[-1]
            begin_x = lines[lines['y1'] == begin_y]['x1'].median()
            begin_x, begin_y = int(begin_x), int(begin_y)
            
            mean_slope = lines["slope"].median()
            end_x = ((end_y - begin_y) / mean_slope) + begin_x
            end_x = int(end_x)
            return ((begin_x, begin_y), (end_x, end_y))

    def polinomial(self, xs, ys, degree):
        return np.polyfit(xs, ys, degree)

    def find_line(self, cropped, is_left):
        lines = self.hough_lines(
            cropped,
        )

        line = self.average_lines(lines, cropped.shape[1], is_left)

        return self.polinomial([line[0][1], line[1][1]], [line[0][0], line[1][0]], 1)

    def find_x(self, line_coefs, y):
        return y * line_coefs[0] + line_coefs[1] 

    def detect_center(self, img):

        gray = self.grayscale(img)
        blurred = self.gaussianBlur(gray)
        low = 60
        high =low*3
        canny = self.canny(blurred, low, high)

        left, right = self.crop(canny)

        left_line_coefs = self.find_line(left, True)
        right_line_coefs = self.find_line(right, False)

        cropped_y = self.roi_left["start_y"] * 1/6

        y_center = int(self.roi_left["start_y"] + cropped_y)

        x_left_cropped = self.find_x(left_line_coefs, cropped_y)
        x_left = x_left_cropped + self.roi_left["start_x"]

        x_right_cropped = self.find_x(right_line_coefs, cropped_y)
        x_right = x_right_cropped + self.roi_right["start_x"]

        x_center = (x_right - x_left) / 2 + x_left
        x_center = int(x_center)

        point_center = (x_center, y_center)

        return point_center, left_line_coefs, right_line_coefs

    def draw_line(self, img, line_coefs, roi):
        cropped_y_top = 0
        cropped_x_top = self.find_x(line_coefs, cropped_y_top)

        cropped_y_bottom = roi["height"]
        cropped_x_bottom = self.find_x(line_coefs, cropped_y_bottom)

        x_top, y_top = cropped_x_top + roi["start_x"], cropped_y_top + roi["start_y"]
        x_bottom, y_bottom = cropped_x_bottom + roi["start_x"], cropped_y_bottom + roi["start_y"]

        x_top, y_top = int(x_top), int(y_top)
        x_bottom, y_bottom = int(x_bottom), int(y_bottom)

        cv2.line(img, (x_top, y_top), (x_bottom, y_bottom), [0,255,0], 10, 4)


if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("-vid", "--isVideo", 
        default=False, 
        help="Specify whether the input is video or image")
    
    ap.add_argument("-i", "--input", required=True,
        help="Specify the input of the image / video")

    ap.add_argument("-o", "--output", 
        default=".",
        help="Specify the output dir")
    
    ap.add_argument("-f", "--file", 
        help="Specify the file name")

    args = vars(ap.parse_args())

    input_path = args["input"]
    filename = args["file"]
    output_dir = args["output"]

    if not args["isVideo"]:
        if filename is None:
            filename = "output_hough.jpg"
        output_path = os.path.join(output_dir, filename)

        img = cv2.imread(input_path)
        origin = img.copy()

        lane_detection = LaneDetectionHough(img.shape[1], img.shape[0], True)
        point_center, left_coefs, right_coefs = lane_detection.detect_center(img)


        cv2.circle(img, point_center, 10, [255, 0, 0], -1)
        lane_detection.draw_line(img, left_coefs, lane_detection.roi_left)
        lane_detection.draw_line(img, right_coefs, lane_detection.roi_right)

        output = stack_images(origin, img)

        cv2.imshow("output", output)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        
        cv2.imwrite(output_path, output)


    else:
        if filename is None:
            filename = "output_hough.avi"
        output_path = os.path.join(output_dir, filename)

        cap = cv2.VideoCapture(input_path)
        fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
        video_width = int(cap.get(3))
        video_height = int(cap.get(4))

        out = cv2.VideoWriter(output_path,fourcc, 40.0, (video_width, int(video_height/2)))

        lane_detection = LaneDetectionHough(video_width, video_height)
        while(cap.isOpened()):
            ret, frame = cap.read()
            if not ret:
                break

            img = frame.copy()

            point_center, left_coefs, right_coefs = lane_detection.detect_center(img)


            cv2.circle(img, point_center, 10, [255, 0, 0], -1)
            lane_detection.draw_line(img, left_coefs, lane_detection.roi_left)
            lane_detection.draw_line(img, right_coefs, lane_detection.roi_right)
            
            cv2.imshow("test", img)

            output = stack_images(frame, img)

            out.write(output)

            cv2.imshow("result", output)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cv2.destroyAllWindows()
        cap.release()
        out.release()


