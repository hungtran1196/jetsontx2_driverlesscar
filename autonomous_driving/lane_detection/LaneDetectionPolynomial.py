#Import dir to python path
import sys, os

def find_parent_path(level=0):
    parent = os.path.abspath(__file__)
    for i in range(level):
        parent = os.path.dirname(parent)

    return parent

sys.path.append(find_parent_path(level=3))


import argparse
import numpy as np
import cv2
import time

from autonomous_driving.lane_detection.utils.BirdViewTransform import BirdViewTransform
from autonomous_driving.lane_detection.utils.ColorDetection import YellowWhiteDetection
from autonomous_driving.lane_detection.utils.utils import stack_images
from config import get_config

# Define a class to receive the characteristics of each line detection
class Line():
    def __init__(self, config):
        self.config = config
        # was the line detected in the last iteration?
        self.detected = False  

        # x values of the last n fits of the line
        self.recent_xfitted = [] 

        #average x values of the fitted line over the last n iterations
        self.bestx = None     

        #polynomial coefficients averaged over the last n iterations
        self.best_fit = None  

        #polynomial coefficients for the most recent fit
        self.current_fit = [] 
         
        #radius of curvature of the line in some units
        self.radius_of_curvature = None 

        #distance in meters of vehicle center from the line
        self.line_base_pos = None 

        #difference in fit coefficients between last and new fits
        self.diffs = np.array([0,0,0], dtype='float') 

        #number of detected pixels
        self.px_count = None

    def add_fit(self, fit, inds):
        # add a found fit to the line, up to n
        if fit is not None:
            if self.best_fit is not None:
                # if we have a best fit, see how this new fit compares
                self.diffs = abs(fit-self.best_fit)

            if (self.diffs[0] > self.config["boundary"]["x0"] or \
                    self.diffs[1] > self.config["boundary"]["x1"] or \
                    self.diffs[2] > self.config["boundary"]["x2"]) and len(self.current_fit) > 0:

                    # bad fit! abort! abort! ... well, unless there are no fits in the current_fit queue, then we'll take it
                    self.detected = False

            else:
                self.detected = True
                self.px_count = np.count_nonzero(inds)
                self.current_fit.append(fit)
                if len(self.current_fit) > self.config["boundary"]["nkeep"]:
                    # throw out old fits, keep newest n
                    self.current_fit = self.current_fit[len(self.current_fit)-self.config["boundary"]["nkeep"]:]
                self.best_fit = np.average(self.current_fit, axis=0)

        # or remove one from the history, if not found
        else:
            self.detected = False
            if len(self.current_fit) > 0:
                # throw out oldest fit
                self.current_fit = self.current_fit[:len(self.current_fit)-1]
            if len(self.current_fit) > 0:
                # if there are still any fits in the queue, best_fit is their average
                self.best_fit = np.average(self.current_fit, axis=0)

class LaneDetectionPolynomial:
    def __init__(self, config=None, isTest=False):
        self.isTest = isTest
        self.config = config
        self.time = {
        }

    def init(self, image):
        (h, w) = image.shape[:2]
        if self.config is None:
            self.config = get_config(w, h)
            self.left_line = Line(config=self.config)
            self.right_line = Line(config=self.config)


        self.color_transform = YellowWhiteDetection()
        self.perspective_transform = BirdViewTransform(image, config=self.config["transform"], isTest=self.isTest)

    def sliding_window_polyfit(self, binary):
           # Take a histogram of the bottom half of the image
        histogram = np.sum(binary[binary.shape[0]//2:,:], axis=0)
        # Find the peak of the left and right halves of the histogram
        # These will be the starting point for the left and right lines
        midpoint = np.int(histogram.shape[0]//2)
        quarter_point = np.int(midpoint//2)
        # Previously the left/right base was the max of the left/right half of the histogram
        # this changes it so that only a quarter of the histogram (directly to the left/right) is considered
        leftx_base = np.argmax(histogram[quarter_point:midpoint]) + quarter_point
        rightx_base = np.argmax(histogram[midpoint:(midpoint+quarter_point)]) + midpoint
        
        #print('base pts:', leftx_base, rightx_base)

        # Choose the number of sliding windows
        nwindows = 10
        # Set height of windows
        window_height = np.int(binary.shape[0]/nwindows)
        # Identify the x and y positions of all nonzero pixels in the image
        nonzero = binary.nonzero()
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        # Current positions to be updated for each window
        leftx_current = leftx_base
        rightx_current = rightx_base
        # Set the width of the windows +/- margin
        margin = 80
        # Set minimum number of pixels found to recenter window
        minpix = 40
        # Create empty lists to receive left and right lane pixel indices
        left_lane_inds = []
        right_lane_inds = []
        # Rectangle data for visualization
        rectangle_data = []

        # Step through the windows one by one
        for window in range(nwindows):
            # Identify window boundaries in x and y (and right and left)
            win_y_low = binary.shape[0] - (window+1)*window_height
            win_y_high = binary.shape[0] - window*window_height
            win_xleft_low = leftx_current - margin
            win_xleft_high = leftx_current + margin
            win_xright_low = rightx_current - margin
            win_xright_high = rightx_current + margin
            rectangle_data.append((win_y_low, win_y_high, win_xleft_low, win_xleft_high, win_xright_low, win_xright_high))
            
            # Identify the nonzero pixels in x and y within the window
            good_left_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & (nonzerox >= win_xleft_low) & (nonzerox < win_xleft_high)).nonzero()[0]
            good_right_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & (nonzerox >= win_xright_low) & (nonzerox < win_xright_high)).nonzero()[0]
            
            # Append these indices to the lists
            left_lane_inds.append(good_left_inds)
            right_lane_inds.append(good_right_inds)
            
            # If you found > minpix pixels, recenter next window on their mean position
            if len(good_left_inds) > minpix:
                leftx_current = np.int(np.mean(nonzerox[good_left_inds]))
            if len(good_right_inds) > minpix:        
                rightx_current = np.int(np.mean(nonzerox[good_right_inds]))

        # Concatenate the arrays of indices
        left_lane_inds = np.concatenate(left_lane_inds)
        right_lane_inds = np.concatenate(right_lane_inds)

        # Extract left and right line pixel positions
        leftx = nonzerox[left_lane_inds]
        lefty = nonzeroy[left_lane_inds] 
        rightx = nonzerox[right_lane_inds]
        righty = nonzeroy[right_lane_inds] 

        left_fit, right_fit = (None, None)
        # Fit a second order polynomial to each
        if len(leftx) != 0:
            left_fit = np.polyfit(lefty, leftx, 2)
        if len(rightx) != 0:
            right_fit = np.polyfit(righty, rightx, 2)
        
        visualization_data = (rectangle_data, histogram)

        return left_fit, right_fit, left_lane_inds, right_lane_inds, visualization_data

    def polyfit_using_prev_fit(self, binary, left_fit_prev, right_fit_prev):
        #Identify the x and y position of non zero pixels in the image
        nonzero = binary.nonzero()
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])

        #Set the margin of each window
        margin = self.config["sliding_windows"]["margin"]

        #Find left and right lane pixels using the previous polynomial
        left_lane_inds = ((nonzerox > (left_fit_prev[0]*(nonzeroy**2) + left_fit_prev[1]*nonzeroy + left_fit_prev[2] - margin)) & 
            (nonzerox < (left_fit_prev[0]*(nonzeroy**2) + left_fit_prev[1]*nonzeroy + left_fit_prev[2] + margin))) 
        right_lane_inds = ((nonzerox > (right_fit_prev[0]*(nonzeroy**2) + right_fit_prev[1]*nonzeroy + right_fit_prev[2] - margin)) & 
            (nonzerox < (right_fit_prev[0]*(nonzeroy**2) + right_fit_prev[1]*nonzeroy + right_fit_prev[2] + margin)))  

        # Again, extract left and right line pixel positions
        leftx = nonzerox[left_lane_inds]
        lefty = nonzeroy[left_lane_inds] 
        rightx = nonzerox[right_lane_inds]
        righty = nonzeroy[right_lane_inds]

        left_fit_new, right_fit_new = (None, None)
        if len(leftx) != 0:
            # Fit a second order polynomial to each
            left_fit_new = np.polyfit(lefty, leftx, 2)
        if len(rightx) != 0:
            right_fit_new = np.polyfit(righty, rightx, 2)

        return left_fit_new, right_fit_new, left_lane_inds, right_lane_inds

    def visualize_slidingWindows(self, binary, left_fit, right_fit, left_lane_inds, right_lane_inds, visualization_data):
        rectangles = visualization_data[0]
        histogram = visualization_data[1]

        # Create an output image to draw on and  visualize the result
        out_img = np.uint8(np.dstack((binary, binary, binary))*255)

        ploty = np.linspace(0, binary.shape[0]-1, binary.shape[0] )
        left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
        right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

        for rect in rectangles:
        # Draw the windows on the visualization image
            cv2.rectangle(out_img,(rect[2],rect[0]),(rect[3],rect[1]),(0,255,0), 2) 
            cv2.rectangle(out_img,(rect[4],rect[0]),(rect[5],rect[1]),(0,255,0), 2) 

        # Identify the x and y positions of all nonzero pixels in the image
        nonzero = binary.nonzero()
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        out_img[nonzeroy[left_lane_inds], nonzerox[left_lane_inds]] = [255, 0, 0]
        out_img[nonzeroy[right_lane_inds], nonzerox[right_lane_inds]] = [100, 200, 255]

        return out_img

    def visualize_polyfit(self, binary, left_fit, right_fit, left_fit2, right_fit2, left_lane_inds2, right_lane_inds2):
        # Generate x and y values for plotting
        ploty = np.linspace(0, binary.shape[0]-1, binary.shape[0] )
        left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
        right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]
        left_fitx2 = left_fit2[0]*ploty**2 + left_fit2[1]*ploty + left_fit2[2]
        right_fitx2 = right_fit2[0]*ploty**2 + right_fit2[1]*ploty + right_fit2[2]

        # Create an image to draw on and an image to show the selection window
        out_img = np.uint8(np.dstack((binary, binary, binary))*255)
        window_img = np.zeros_like(out_img)

        # Color in left and right line pixels
        nonzero = binary.nonzero()
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        out_img[nonzeroy[left_lane_inds2], nonzerox[left_lane_inds2]] = [255, 0, 0]
        out_img[nonzeroy[right_lane_inds2], nonzerox[right_lane_inds2]] = [0, 0, 255]

        # Generate a polygon to illustrate the search window area (OLD FIT)
        # And recast the x and y points into usable format for cv2.fillPoly()
        left_line_window1 = np.array([np.transpose(np.vstack([left_fitx-self.config["sliding_windows"]["margin"], ploty]))])
        left_line_window2 = np.array([np.flipud(np.transpose(np.vstack([left_fitx+self.config["sliding_windows"]["margin"], ploty])))])
        left_line_pts = np.hstack((left_line_window1, left_line_window2))
        right_line_window1 = np.array([np.transpose(np.vstack([right_fitx-self.config["sliding_windows"]["margin"], ploty]))])
        right_line_window2 = np.array([np.flipud(np.transpose(np.vstack([right_fitx+self.config["sliding_windows"]["margin"], ploty])))])
        right_line_pts = np.hstack((right_line_window1, right_line_window2))

        # Draw the lane onto the warped blank image
        cv2.fillPoly(window_img, np.int_([left_line_pts]), (0,255, 0))
        cv2.fillPoly(window_img, np.int_([right_line_pts]), (0,255, 0))
        result = cv2.addWeighted(out_img, 1, window_img, 0.3, 0)

        return result

    def find_lanes(self, img):
        start_time = time.time()
        transformed, origin = self.perspective_transform.transform(img)
        self.time["preprocess_perspective"] = time.time() - start_time

        start_time = time.time()
        binary = self.color_transform.detect(transformed)
        self.time["preprocess_binary"] = time.time() - start_time

        start_time = time.time()
        boxes = None
        if not self.left_line.detected or not self.right_line.detected:
            l_fit, r_fit, l_lane_inds, r_lane_inds, boxes = self.sliding_window_polyfit(binary)
        else:
            l_fit, r_fit, l_lane_inds, r_lane_inds = self.polyfit_using_prev_fit(binary, self.left_line.best_fit, self.right_line.best_fit)

        # invalidate both fits if the difference in their x-intercepts isn't around 350 px (+/- 100 px)
        if l_fit is not None and r_fit is not None:
            # calculate x-intercept (bottom of image, x=image_height) for fits
            h = img.shape[0]
            l_fit_x_int = l_fit[0]*h**2 + l_fit[1]*h + l_fit[2]
            r_fit_x_int = r_fit[0]*h**2 + r_fit[1]*h + r_fit[2]
            x_int_diff = abs(r_fit_x_int-l_fit_x_int)

            if abs(self.config["boundary"]["lane_pixel_diff"] - x_int_diff) > self.config["boundary"]["lane_pixel_diff_error"]:
                l_fit = None
                r_fit = None
            
        self.left_line.add_fit(l_fit, l_lane_inds)
        self.right_line.add_fit(r_fit, r_lane_inds)
        self.time["process"] = time.time() - start_time

        (centerx, centery) = self.find_center()

        if self.isTest:
            sliding_img = None
            if (boxes and (l_fit is not None) and (r_fit is not None)):
                sliding_img = self.visualize_slidingWindows(binary, l_fit, r_fit, l_lane_inds, r_lane_inds, boxes)
            elif (l_fit is not None) and (r_fit is not None):
                sliding_img = self.visualize_polyfit(binary, self.left_line.best_fit, self.right_line.best_fit, l_fit, r_fit, l_lane_inds, r_lane_inds)
            else:
                sliding_img = np.dstack((binary, binary, binary))
            binary = np.dstack((binary, binary, binary))

            binary_fit = binary.copy()
            for i, fit in enumerate(self.left_line.current_fit):
                binary_fit = self.plot_fit_onto_img(binary_fit, fit, (20*i+100,0,20*i+100))
            for i, fit in enumerate(self.right_line.current_fit):
                binary_fit = self.plot_fit_onto_img(binary_fit, fit, (0,20*i+100,20*i+100))
            binary_fit = self.plot_fit_onto_img(binary_fit, self.left_line.best_fit, (255,255,0))
            binary_fit = self.plot_fit_onto_img(binary_fit, self.right_line.best_fit, (255,255,0))

            (h, w) = binary_fit.shape[:2]
            binary_fit = cv2.arrowedLine(binary_fit, (w//2, h), (centerx, centery), (0, 255,255), 10)

            tmp_img_top = stack_images(origin, transformed)
            tmp_img_bottom = stack_images(sliding_img, binary_fit)


            diag_img = stack_images(tmp_img_top, tmp_img_bottom, axis=1, resize=False)

            # diagnostic data (bottom left)
            color_ok = (200,255,155)
            color_bad = (255,155,155)
            font = cv2.FONT_HERSHEY_DUPLEX

            textx, texty = self.config["text_img_polynomial"]["x"], self.config["text_img_polynomial"]["y"]
            offset = self.config["text_img_polynomial"]["offset"]

            if l_fit is not None:
                text = 'This fit L: ' + ' {:0.6f}'.format(l_fit[0]) + \
                                        ' {:0.6f}'.format(l_fit[1]) + \
                                        ' {:0.6f}'.format(l_fit[2])
            else:
                text = 'This fit L: None'
            cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)
            if r_fit is not None:
                text = 'This fit R: ' + ' {:0.6f}'.format(r_fit[0]) + \
                                        ' {:0.6f}'.format(r_fit[1]) + \
                                        ' {:0.6f}'.format(r_fit[2])
            else:
                text = 'This fit R: None'
            
            texty = texty + offset
            cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)
            
            texty = texty + offset * 2
            if self.left_line.best_fit is not None:
                text = 'Best fit L: ' + ' {:0.6f}'.format(self.left_line.best_fit[0]) + \
                                        ' {:0.6f}'.format(self.left_line.best_fit[1]) + \
                                        ' {:0.6f}'.format(self.left_line.best_fit[2])
                cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)
            
            texty = texty + offset 
            if self.right_line.best_fit is not None:
                text = 'Best fit R: ' + ' {:0.6f}'.format(self.right_line.best_fit[0]) + \
                                        ' {:0.6f}'.format(self.right_line.best_fit[1]) + \
                                        ' {:0.6f}'.format(self.right_line.best_fit[2])
                cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)
            
            text = 'Diffs L: ' + ' {:0.6f}'.format(self.left_line.diffs[0]) + \
                                ' {:0.6f}'.format(self.left_line.diffs[1]) + \
                                ' {:0.6f}'.format(self.left_line.diffs[2])
            
            if self.left_line.diffs[0] > 0.001 or \
            self.left_line.diffs[1] > 1.0 or \
            self.left_line.diffs[2] > 100.:
                diffs_color = color_bad
            else:
                diffs_color = color_ok
            
            texty = texty + offset * 2
            cv2.putText(diag_img, text, (textx,texty), font, .5, diffs_color, 1, cv2.LINE_AA)
            
            text = 'Diffs R: ' + ' {:0.6f}'.format(self.right_line.diffs[0]) + \
                                ' {:0.6f}'.format(self.right_line.diffs[1]) + \
                                ' {:0.6f}'.format(self.right_line.diffs[2])

                
            if self.right_line.diffs[0] > 0.001 or \
            self.right_line.diffs[1] > 1.0 or \
            self.right_line.diffs[2] > 100.:
                diffs_color = color_bad
            else:
                diffs_color = color_ok

            texty = texty + offset
            cv2.putText(diag_img, text, (textx,texty), font, .5, diffs_color, 1, cv2.LINE_AA)
            text = 'Good fit count L:' + str(len(self.left_line.current_fit))
            
            texty = texty + offset * 2
            cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)
            
            texty = texty + offset
            text = 'Good fit count R:' + str(len(self.right_line.current_fit))
            cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)

            #Put time on the image
            texty = texty + offset * 2
            text = "perspective transform time: " +  str(self.time["preprocess_perspective"])
            cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)
            
            texty = texty + offset
            text = "binary transform time: " +  str(self.time["preprocess_binary"])
            cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)
            texty = texty + offset
            text = "Process time: " +  str(self.time["process"])
            cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)
            texty = texty + offset
            text = "fps: " +  str(1/(self.time["process"] + self.time["preprocess_binary"] + self.time["preprocess_perspective"]))
            cv2.putText(diag_img, text, (textx,texty), font, .5, color_ok, 1, cv2.LINE_AA)

            return (centerx, centery), diag_img

        return (centerx, centery), None

    def draw_lane(self, original_img, binary_img, l_fit, r_fit, Minv):
        h, w = original_img.shape[:2]
        new_img = np.copy(original_img)
        if l_fit is None or r_fit is None:
            return original_img
        # Create an image to draw the lines on
        warp_zero = np.zeros_like(binary_img).astype(np.uint8)
        color_warp = np.dstack((warp_zero, warp_zero, warp_zero))
        
        h,w = binary_img.shape
        ploty = np.linspace(0, h-1, num=h)# to cover same y-range as image
        left_fitx = l_fit[0]*ploty**2 + l_fit[1]*ploty + l_fit[2]
        right_fitx = r_fit[0]*ploty**2 + r_fit[1]*ploty + r_fit[2]

        # Recast the x and y points into usable format for cv2.fillPoly()
        pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
        pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
        pts = np.hstack((pts_left, pts_right))

        # Draw the lane onto the warped blank image
        cv2.fillPoly(color_warp, np.int_([pts]), (0,255, 0))
        cv2.polylines(color_warp, np.int32([pts_left]), isClosed=False, color=(255,0,255), thickness=15)
        cv2.polylines(color_warp, np.int32([pts_right]), isClosed=False, color=(0,255,255), thickness=15)

        # Warp the blank back to original image space using inverse perspective matrix (Minv)
        newwarp = cv2.warpPerspective(color_warp, self.perspective_transform.Minv, (w, h)) 
        # Combine the result with the original image
        result = cv2.addWeighted(new_img, 1, newwarp, 0.5, 0)
        return result
            
    def plot_fit_onto_img(self, img, fit, plot_color):
        if fit is None:
            return img

        h, w = img.shape[:2]

        new_img = np.copy(img)
        h = new_img.shape[0]
        ploty = np.linspace(0, h-1, h)
        plotx = fit[0]*ploty**2 + fit[1]*ploty + fit[2]
        pts = np.array([np.transpose(np.vstack([plotx, ploty]))])
        cv2.polylines(new_img, np.int32([pts]), isClosed=False, color=plot_color, thickness=8)
        return new_img

    def find_center(self):
        centery = self.config["center"]["y"]
        offset = self.config["center"]["offset"]

        if self.left_line.best_fit is not None and self.right_line.best_fit is not None:
            center_line = (self.left_line.best_fit + self.right_line.best_fit) / 2
            centerx = center_line[0]*(centery**2) + center_line[1]*centery + center_line[2]
                        
            return (int(centerx), int(centery))
        
        elif self.left_line.best_fit is not None:
            leftx =  self.left_line.best_fit[0]*(centery**2) + self.left_line.best_fit[1]*centery + self.left_line.best_fit[2]

            return (int(leftx + offset), int(centery))

        elif self.right_line.best_fit is not None:
            rightx =  self.right_line.best_fit[0]*(centery**2) + self.right_line.best_fit[1]*centery + self.right_line.best_fit[2]

            return (int(rightx - offset), int(centery))

        else:
            return (self.config["transform"]["width"]//2, 0)

if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("-vid", "--isVideo", 
        default=False, 
        help="Specify whether the input is video or image")
    
    ap.add_argument("-i", "--input", required=True,
        help="Specify the input of the image / video")

    ap.add_argument("-o", "--output", 
        default=".",
        help="Specify the output dir")
    
    ap.add_argument("-f", "--file", 
        help="Specify the file name")



    args = vars(ap.parse_args())

    input_path = args["input"]
    filename = args["file"]
    output_dir = args["output"]

    if not args["isVideo"]:
        if filename is None:
            filename = "output.jpg"
        output_path = os.path.join(output_dir, filename)

        img = cv2.imread(input_path)

        detector = LaneDetectionPolynomial(isTest=True)
        detector.init(img)

        center, output = detector.find_lanes(img)

        cv2.imshow("output", output)

        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite(output_path, output)

    else:
        if filename is None:
            filename = "output.avi"
        output_path = os.path.join(output_dir, filename)

        cap = cv2.VideoCapture(input_path)
        fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
        video_width = int(cap.get(3))
        video_height = int(cap.get(4))

        out = cv2.VideoWriter(output_path,fourcc, 40.0, (video_width, video_height))
        detector = LaneDetectionPolynomial(isTest=True)
        init = True

        while(cap.isOpened()):
            ret, frame = cap.read()
            if not ret:
                break

            if init:
                detector.init(frame)
                init = False

            center, output = detector.find_lanes(frame)

            cv2.imshow("output", output)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            out.write(output)

        cap.release()
        out.release()
    



