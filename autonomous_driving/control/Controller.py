import math
import sys, os


def find_parent_path(level=0):
    parent = os.path.abspath(__file__)
    for i in range(level):
        parent = os.path.dirname(parent)

    return parent

sys.path.append(find_parent_path(level=3))

from gpio_controller.ServoController import ServoController

class Controller:
    def __init__(self):
        self.servo_controller = ServoController()

    def init(self, speed, center):
        self.servo_controller.init()
        self.center = center
        self.base_speed = speed


    def get_theta(next):
        (x, y) = next
        (centerx, centery) = this.center
        dx = x - centerx
        dy = abs(centery - y)

        return math.atan(dx / dy)

    def get_speed(self):
        #TODO NEXT: Implement dynamic speed
        #Current implementation is static speed

        return self.base_speed

    def drive(self, next_point):
        #TODO NEXT: Implement PID controll
        #Current is normal controll

        theta = self.get_theta(next_point)
        speed = self.get_speed()

        self.servo_controller.set_steer(theta)
        self.servo_controller.set_speed(speed)

    def stop(self):
        self.servo_controller.set_steer(0)
        self.servo_controller.set_speed(0)

