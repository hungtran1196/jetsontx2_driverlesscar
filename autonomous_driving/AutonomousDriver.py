import math
import sys, os
import cv2
import argparse

def find_parent_path(level=0):
    parent = os.path.abspath(__file__)
    for i in range(level):
        parent = os.path.dirname(parent)

    return parent

sys.path.append(find_parent_path(level=2))

from camera_handler.CameraCalibration import CameraCalibration
from camera_handler.CameraHandler import CameraHandler
from autonomous_driving.control.Controller import Controller
from autonomous_driving.lane_detection.LaneDetectionPolynomial import LaneDetectionPolynomial
from config import get_config

import signal

class AutonomousDriver:
    def __init__(self, base_speed=40, config=None, lane_detection=None, isTest=False, isSave=False):
        self.isTest = isTest
        self.isSave = isSave
        self.saver = None
        self.originSaver = None
        
        if self.isSave:
            self.isTest = True

        self.config = config
        if lane_detection is None:
            self.lane_detection = LaneDetectionPolynomial(config=config, isTest=self.isTest)
        
        self.controller = Controller()
        self.camera = CameraHandler()
        
        self.base_speed = base_speed

        #TODO: Implement camera calibration - not yet implemented
        #self.camera_calibration = CameraCalibration()

    def init(self, init_frame):
        (h, w) = init_frame.shape[:2]
        if self.config is None:
            self.config = get_config(w, h)
            self.controller.init(speed=self.base_speed, center=(w//2, h))
            self.lane_detection.init(init_frame)

        if self.isSave:
            fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
            self.saver = cv2.VideoWriter("./auto_driver.avi",fourcc, 40.0, (w, h))
            self.originSaver = cv2.VideoWriter("./auto_driver_origin.avi",fourcc, 40.0, (w, h))


    def run(self, fn=None):
        init = True
        isPause = False
        while True:
            frame = self.camera.get_frame()

            if init:
                self.init(frame)
                init = False

            if self.isSave:
                self.originSaver.write(frame)

            center_point, img = self.lane_detection.find_lanes(frame)

            if fn is not None:
                isStop = fn(img)
                if isStop:
                    self.stop()
                    print("Stop")
                    break

            if self.isSave:
                self.saver.write(img)

            # cv2.imshow("output", img)
            # if cv2.waitKey(1) & 0xFF == ord('q'):
            #     break

            # self.controller.drive(center_point)

        # self.controller.stop()
        self.stop()

    def stop(self):
        self.controller.stop()
        if(self.isSave):
            self.saver.release()
            self.originSaver.release()
        print("Stopping")
        sys.exit()

def show_img(img):
    cv2.imshow("output", img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        return True


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--isSave", 
        default=False, 
        help="Specify whether the driver save output to video or not")

    ap.add_argument("-t", "--isTest", 
        default=False, 
        help="Specify whether the driver save output to video or not")


    ap.add_argument("-bs", "--baseSpeed", 
        default=40, 
        help="Specify the base speed of the car")

    args = vars(ap.parse_args())


    driver = AutonomousDriver(isSave=args["isSave"], isTest=args["isTest"], base_speed=args["baseSpeed"])
    try:
        if (args["isTest"]):
            driver.run(show_img)
        else:
            driver.run()
    except KeyboardInterrupt:
        driver.stop()
        sys.exit()

    
    signal.signal(signal.SIGINT, lambda x: driver.stop())



        
